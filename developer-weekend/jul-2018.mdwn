[[!meta title="Developer Weekend (July 2018)"]]
[[!meta author="Daniel Silverstone"]]
[[!meta date="2018-07-28 12:00:00"]]

[[!toc]]

Attendees
=========

* Michael Drake
* Vincent Sanders
* Daniel Silverstone

Apologies
=========

* John-Mark Bell
* Chris Young

Topics
======

* Use `libnsfb` as a backing tool for SVG, thumbnailing, and Canvas rendering?
* Revisit _old_ bugs on the tracker and close down stuff which is definitely
  not happening.
* Update CI to turn on santizers for builds on supported platforms.
* Update CI to ensure `DEBUG` level _nslog_ stuff is compiled in for CI builds.
* Toolchain patches need examining.  Consider if we can build with a more
  modern OS for Atari. _Sadly we missed this one_

Activity
========

Bug Triage
----------

* Group bug for line breaking issues is [[!bug 467]]

Daniel
------

* Added `sanitize` as a target to the top level makefiles in the buildsystem.
    * _Usage:_ `make VARIANT=debug clean; make VARIANT=debug sanitize`
    * If you subsequently want the sanitize-enabled library installed do:
      `make VARIANT=debug install`
    * __NOTE__: If you do that, you'll need to always run the sanitize targets
      for any rdepends.
* Spotted a bug in the `libwapcaplet` test suite, fixed that.
* Added `sanitize` target to the main NetSurf makefiles including nice config
  overrides.  For main docs, see `Makefile.config.example` but failing that you
  can run the test suite in sanitizer mode with: `make sanitize`
* Made it so that CI builds of NetSurf set the `libnslog` compile-in level to
  `DEBUG`
* Make sanitizer changes to `libsvgtiny`, `libnsbmp`, `libnsgif` etc.
* Made a bunch of changes to `libdom` to clean up, it's still not `sanitize`
  clean sadly.

Michael
-------

* Helped on [[!bug 2606]]
* Looked at sanitizer issues.

Vincent Sanders
---------------

* Added sanitizer targets for some libraries, but....
* Spent quite a while fettling the CI workers so that they will, erm, CI work.
* Triaged bunch of line-breaking related bugs on the tracker

Frontends
=========

We revisited the decisions made in [January](../jan-2018/) and
decided they're all good so we're not changing them for now.

Next time
=========

We have chosen the next developer weekend to be November 2/3/4 2018.
It shall be at Avon Road in Manchester

