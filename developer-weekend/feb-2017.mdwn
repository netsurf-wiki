[[!meta title="Developer Weekend (Feb 2017)"]]
[[!meta author="Daniel Silverstone"]]
[[!meta date="2017-02-04 12:00:00"]]

[[!toc]]

Attendees
=========

* John-Mark Bell (Yes, he's not dead) (Still!)
* Michael Drake
* Vincent Sanders
* Daniel Silverstone

Apologies
=========

* Chris Young (Still not quite up to visiting)

Topics
======

GIF Stuff
---------

John-Mark and Vince have had a trawl through the `libnsgif` stuff and
basically decided "OH GODS NO!" and want to replace the lzw stuff.

Content Sharing
---------------

We decided that any content which implicitly or explicitly contains context
(for example HTML, Animated GIFs, audio, video, etc) cannot be shared.

GIFs could start out unshared, and if they determine later that they are not
animated, they could flip to a shared context.  Ditto they could, on
`CONTENT_DONE`, switch from copied data to referencing the whole content stored
in `llcache`.

Auto-Updating Contents
----------------------

Contents which update automagically (e.g. animated gifs) should, when an update
occurs, send a message up to the content's users to notify that a change has
happened. It is then left up to the users to decide what to do about it.

Thus, for animated GIFs, which update off the scheduler, when the scheduled
callback happens, the content handler should update its frame counter, and
send a content changed message. On receipt of the message, the relevant user(s)
should decide what to do (if anything -- if the GIF's not visible, there's no
point decoding the new frame and attempting to display it).

CSS Media Queries
-----------------

John-Mark is getting on with these

Actions:

* Parser implementation 90% complete (other 90% remains to be done)
* No selection implementation as yet (although Michael has defined a
  mechanism for libcss clients to provide media state at selection time)

Plotter API work
----------------

After discussion, Vince is working on updating the plotter API to thread the
plot context through everything, for widgetery (or however you spell that)

Actions:

* define plotter API which passes context to each call **MERGED**
* update all core uses of API to match changes **MERGED**
* update core plotters (knockout and print) to meet new API **MERGED**
* update each frontend plotters to provide API **MERGED**

New layout engine: `libnslayout`
--------------------------------

Michael started looking at the new layout engine again.

* Recalled the state of things when it was last touched.
* Cleaned up what's there.
* The next step for development is to add CSS selection support.  Following
  that we can actually experiment with layout.
* Reviewed the current state of the specs that this library needs to implement.
  * Decided to focus on CSS2.2 first, and worry about level 3/4 stuff later,
    since they can be treated mostly as extras.
  * Downloaded a copy of the current CSS2.2 document.
* Ongoing: design the new layout engine.

Cookies
-------

netsurf cookies currently adheres to RFC2109 (kinda) and we should implement
RFC6265

Vince has investigated and this is necessary for using cookies at least
partially safely with javascript. without this implementation javascript access
is not secure.

JavaScript and Events
---------------------

Daniel continues his epic horror-party into `addEventListener()` and friends.

Activity:

* Fixed bug in nsgenbind which meant generic onFOO handlers were always being
  registered for 'click'. **MERGED**
* Added support for event listeners to duktape binding **MERGED**
* Added `EventTarget` IDL implementation to duktape binding **MERGED**
* Added test case for `DOMNodeInserted` which sadly doesn't work on modern
  browsers due to them switching to mutation observers. **MERGED**
* Updated `Docs/UnimplementedJavascript.txt` for `EventTarget` **MERGED**
* Added an alignment to shush clang for struct casting **MERGED**

Tests
-----

Daniel found and corrected some hiccoughs in the `test/` tree:

* We were linking the system libidn rather than using `utils/punycode.c`
  **MERGED**
* We were at risk of going over-time on some hash table tests.
  **MERGED**

Frontends
---------

* **cocoa:** to be put on notice. noone is maintaining it. if noone responds on
  developer or user list then CI will stop building and updating to build will
  not be required. If the frontend does not build at the next developer weekend
  it will be removed from git.

* **atari:** toolchains are badly in need of a refresh. will be removed if
  noone wants to maintain it.

Next time
=========

We have chosen the next developer weekend to be June the 9th/10th/11th 2017.
It shall be at the same place as this one (Males Close)
