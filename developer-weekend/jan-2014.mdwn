[[!meta title="Developer Weekend (Jan 2014)"]]
[[!meta author="Kyllikki"]]
[[!meta date="2013-12-18T11:25:18Z"]]


[[!toc]]

When
----

To be held afternoon of 3rd to afternoon of 5th

Who
---

Anyone interested in contributing to NetSurf, we generally have small
discussion times and a great deal of hacking time. Previous events have
crammed in excess of 20 hours working time into the weekend.

You **must** confirm your attendance no later than the 2nd of January
2014 so i can update the security list. We cannot offer travel
sponsorship to the event and free accommodation is limited (and basic).

Currently confirmed are

-   Vincent Sanders
-   Michael Drake
-   Daniel Silverstone
-   Rob Kendrick
-   John Mark-Bell

Where
-----

At the Collabora Cambridge offices: Kett house Station Road Cambridge

CB1 2JH

<http://goo.gl/maps/iOb9R>

This venue has easy access from the railway station and is a twenty
minute walk from the city cenre where national express coaches stop.
Nearest airport is Stanstead and the railway provides good links.

Vincent has arranged to have accommodation for two people. More might be
accommodated if they do not mind sleeping bags on the floor. Vincent
also has a car and will be designated driver.

Agenda
------

List of ideas to consider and/or work on, if they've not already been
addressed:

-   Forms to use LibDOM
-   New layout engine discussion
-   State of JS discussion
-   More LibCSS work (style interning & sharing)
-   Release process was a mad rush for 3.0
    -   Do we need to sort anything out?
    -   Non RISC OS platforms still haven't had a 3.0 release on our
        site.
    -   Sort out the autobuilder generating releases
-   Can LibDOM be optimised?
-   Hubbub is years behind spec, and causing real bugs.
-   media and GStreamer 1.0 integration
-   libjpeg turbo
-   haru pdf integration
-   printing
-   Any other 3.1 blockers?
-   Urgent bugs?

