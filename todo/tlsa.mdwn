[[!meta title="Todo/tlsa"]]
[[!meta author="Tlsa"]]
[[!meta date="2011-09-07T14:12:14Z"]]


[[!toc]] Michael's todo list

NetSurf
-------

### Render

-   Form widget overhaul, treat as replaced elements and use core
    textarea

### Layout

-   If specified table height is greater than used height, share extra
    height over rows
-   Stacking, z-index

### Core stuff

-   Status bar updates
-   Pointer image updates
-   Move input handling to content handlers
-   Debug window
-   Window showing all the errors encountered when loading a page

Web site
--------

-   About team page. Add zamez info, new folk (porters, GSoC folk who
    stay)
-   Adverts section with banners, buttons and mag. ads.

Releases
--------

-   Update homepage
-   Update download page
-   Update about NS page
-   Update screenshots page
-   Announce on user and dev MLs
-   Announce on csa.a, various RO software DB sites. ANS filebase thing.
-   There are various DBs of Linux software to get NS submitted to

