[[!meta title="Documentation/Treeview"]]
[[!meta author="James Bursa"]]
[[!meta date="2010-03-01T02:57:03Z"]]


[[!toc]] Using the treeview gadget
(together with core hotlist/history/cookies/sslcert)

Providing front end specific bits
---------------------------------

Most of the treeview logic is located in core code but there still are
some functions that need implementing on the front end side. These
include:

-   Functions marked as front end specific ones and listed at the end of

` tree_url_node.h.`

-   Functions in treeview\_table located in tree.h

Additionally the option\_tree\_icons\_dir option should be set. It
should be pointing at the directory containing the frontend specific
icon set for the treeview.

During GUI initialisation the following functions should be called:

-   cookies\_initialise
-   history\_global\_initialise
-   hotlist\_initialise
-   sslcert\_init

On GUI closure:

-   cookies\_cleanup
-   history\_global\_cleanup
-   hotlist\_cleanup
-   no function for sslcert here!

The sslcert tree is created and deleted each time the dialog gets
invoked. This is done with sslcert\_create\_tree and sslcert\_cleanup.
Make sure that closing the window with any other method will also
perform the cleanup.

The remaining functions in the header files of
hotlist/history/cookies/sslcert have to be connected to the front end
specific buttons and menu or tool bars.

The front end is responsible for creating the windows and toolbars as
well as empty core trees passed to the cookies/history/hotlist/sslcert
initialisation functions. Use the \*\_get\_tree\_flags functions to
obtain the flags with which the particular tree should be created.

Using the treeview
------------------

### Creating and modifying the tree structure

-   Create an empty tree with tree\_create.
-   Create the necessary folder/leave nodes.
-   Fill the leave nodes with tree\_create\_node\_element and assign
    content to the elements with tree\_update\_node\_element.
-   Use tree\_link\_node to connect the created nodes in the required
    structure.
-   With tree\_set\_redraw set the redraw flag on the tree to true, any
    further changes will result in tree redraws. The flag can be
    set/unset multiple times.
-   Call tree\_draw to draw the tree for the first time.

### Client -\> Tree communication

To make the tree respond to mouse action, all such has to be passed to
tree\_mouse\_action and tree\_drag\_end. The mouse coordinates have to
be given with respect to the tree origin.

### Tree -\> Client communication

Use tree\_set\_node\_user\_callback to provide a callback for any node
related events. At the moment it can be used for(default behavior for a
not handled event in brackets):

-   handling a node launch (no action)
-   custom node element deletion (free text/bitmap)
-   accepting/rejecting edited text (any text accepted)
-   informing about edit end (no action)

[[!inline raw=yes pages="Documentation"]]

