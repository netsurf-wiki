[[!meta title="Module:utils/container"]]
[[!meta author="James Bursa"]]
[[!meta date="2011-01-09T22:28:35Z"]]


[[!toc]] File format for NetSurf
themes.

:   ''Note: possibly incomplete; no themes currently exist.

Prefix
------

`container_`

Depends on
----------

-   [[Module:utils/log|module/utils/log]]
-   [[Module:utils/messages|module/utils/messages]]
-   [[Module:utils/utils|module/utils/utils]]

Description
-----------

This module implements a simple file format which contains a series of
files (i.e. named chunks of data).

Files
-----

utils/container.h
:   Interface
utils/container.c
:   Implementation

[[!inline raw=yes pages="Module:Index"]]

