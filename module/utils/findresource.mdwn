[[!meta title="Module:utils/findresource"]]
[[!meta author="James Bursa"]]
[[!meta date="2011-01-09T22:37:11Z"]]


[[!toc]] Finds resource files in a
list of locations.

Prefix
------

Does not conform to convention; see header.

Depends on
----------

None.

Description
-----------

This module supplies functions to find files in a list of directories.
This is used to load resource files, for example looking in the home
directory first, and then other standard locations.

Files
-----

utils/findresources.h
:   Interface
utils/findresources.c
:   Implementation

[[!inline raw=yes pages="Module:Index"]]

