[[!meta title="Continuous Integration Mac OS X Snow Leopard Setup"]]
[[!meta author="Kyllikki"]]
[[!meta date="2016-02-06T11:52:56Z"]]


[[!toc]]

Manual Installation
-------------------

Install snow leopard from master discs

Create jenkins user

Install XCode

Software update to latest version

Enable remote login (sshd)

if xcode 4 is installed you need to do:

`sudo xcodebuild -license`

Install [[https://code.google.com/p/tunnelblick/ tunnelblick|https///code.google.com/p/tunnelblick/_tunnelblick]] and configure for
auto start openvpn to ci master

Download Mac Ports:

`curl -o MacPorts-2.3.1-10.6-SnowLeopard.pkg `[`https://distfiles.macports.org/MacPorts/MacPorts-2.3.1-10.6-SnowLeopard.pkg`](https://distfiles.macports.org/MacPorts/MacPorts-2.3.1-10.6-SnowLeopard.pkg)

double click on the pkg file to install

Update ports database ready for use

`export PATH=/opt/local/bin:/opt/local/sbin:$PATH`  
`sudo port -v selfupdate`

install required packages

`sudo port install git ccache expat openssl curl libjpeg-turbo libpng p5-html-parser screen bison`

As jenkins user:

create jenkins workspace directory

`mkdir jenkins`

download slave jar

`curl -o slave.jar `[`http://ci.netsurf-browser.org/jenkins/jnlpJars/slave.jar`](http://ci.netsurf-browser.org/jenkins/jnlpJars/slave.jar)

Add node on jenkins master CI instance

`Name       cislave7`  
`Description    Mac os X build slave`  
`# of executors 1`  
`Remote FS root /Users/jenkins/jenkins`  
`Labels     i686-apple-darwin10`

take note of secret for use.

create jenkins slave script

`cat << EOF > jenkins-slave.sh`  
`#!/bin/sh`  
  
`java -Djava.awt.headless=true -jar slave.jar -jnlpUrl `[`http://ci.netsurf-browser.org/jenkins/computer/cislave7/slave-agent.jnlp`](http://ci.netsurf-browser.org/jenkins/computer/cislave7/slave-agent.jnlp)` -secret 1234`  
`EOF`  
`chmod a+x jenkins-slave.sh`

create ssh keypair (accept defaults - no password)

`ssh-keygen -t rsa -C "netsurf@cislave8.netsurf-browser.org"`

copy .ssh/id\_rsa.pub from slave to jenkins master node and append to
/home/netsurf/.ssh/authorized\_keys

start slave daemon in screen

