[[!meta title="Test/LibCSS/NStests"]]
[[!meta author="Jmb"]]
[[!meta date="2009-07-16T14:01:30Z"]]


[[!toc]] Testing the NetSurf port
to LibCSS for regressions against the netsurftest/works test suite.

    3d-border-colour.html ---------------------------------------------- OK
    absolute-container-assertion.html ---------------------------------- OK
    absolute.html ------------------------------------------------------ OK
    absolute-padding.html ---------------------------------------------- OK
    absolute-position-first-child-of-containing-block.html ------------- OK
    absolute-position-table.html --------------------------------------- OK
    amazon-2.html ------------------------------------------------------ OK
    amazon.html -------------------------------------------------------- OK
    anim-display-none.html --------------------------------------------- OK
    animgif.html ------------------------------------------------------- OK
    animgifobj.html ---------------------------------------------------- OK
    artworks-transparency.html ----------------------------------------- OK
    background-edge.html ----------------------------------------------- OK
    background-image-failure2.html ------------------------------------- OK
    background-image-failure3.html ------------------------------------- OK
    background-image-failure.html -------------------------------------- OK
    background-on-TEXT.html -------------------------------------------- OK
    background-position-repeat.html ------------------------------------ OK
    background-redraw-bug.html ----------------------------------------- OK
    block-formatting-context-top-margin.html --------------------------- OK
    body-margin-padding-bgimage-test.html ------------------------------ OK
    border-background.html --------------------------------------------- OK
    borders.html ------------------------------------------------------- OK
    box-at-point-test.html --------------------------------------------- OK
    br.html ------------------------------------------------------------ OK
    bunnies.html ------------------------------------------------------- OK
    button-height.html ------------------------------------------------- OK
    button-width.html -------------------------------------------------- OK
    cellpadding-csspadding.html ---------------------------------------- OK
    cellpadding.html --------------------------------------------------- OK
    clear-on-float2.html ----------------------------------------------- OK
    clear-on-float.html ------------------------------------------------ OK
    collapse.html ------------------------------------------------------ OK
    colspan-issue.html ------------------------------------------------- OK
    containing-block.html ---------------------------------------------- OK
    csszen1.html ------------------------------------------------------- OK
    default-page-margin--no-body-margin-test.html ---------------------- OK
    default-page-margin--no-body-padding-test.html --------------------- OK
    default-page-margin--no-html-margin-test.html ---------------------- OK
    default-page-margin--no-html-padding-test.html --------------------- OK
    default-page-margin--test.html ------------------------------------- OK
    digglayout.html ---------------------------------------------------- OK
    div-and-table-width-tests.html ------------------------------------- OK
    drawfile.html ------------------------------------------------------ OK
    float-absolute.html ------------------------------------------------ OK
    float-based-layout.html -------------------------------------------- OK
    float-clear2.html -------------------------------------------------- OK
    float-clear3.html -------------------------------------------------- OK
    float-clear4.html -------------------------------------------------- OK
    float-clear5.html -------------------------------------------------- OK
    float-clear6.html -------------------------------------------------- OK
    float-clear7.html -------------------------------------------------- OK
    float-clear-block-formatting-context.html -------------------------- OK
    float-clear-first2.html -------------------------------------------- OK
    float-clear-first3.html -------------------------------------------- OK
    float-clear-first.html --------------------------------------------- OK
    float-clear.html --------------------------------------------------- OK
    float-clearing.html ------------------------------------------------ OK
    float-clearing-issue.html ------------------------------------------ OK
    floated-list-items.html -------------------------------------------- OK
    float-gaps2.html --------------------------------------------------- OK
    float-gaps.html ---------------------------------------------------- OK
    float-layout.html -------------------------------------------------- OK
    float-li.html ------------------------------------------------------ OK
    floatmargin.html --------------------------------------------------- OK
    float-negative-margin.html ----------------------------------------- OK
    float-nested-table.html -------------------------------------------- OK
    float-overlap.html ------------------------------------------------- OK
    floatpadding.html -------------------------------------------------- OK
    float-positioning.html --------------------------------------------- OK
    floats-and-position.html ------------------------------------------- OK
    floats-block-formatting-context2.html ------------------------------ OK
    floats-block-formatting-context3.html ------------------------------ OK
    floats-block-formatting-context4.html ------------------------------ OK
    floats-block-formatting-context5.html ------------------------------ OK
    floats-block-formatting-context6.html ------------------------------ OK
    floats-block-formatting-context7.html ------------------------------ OK
    floats-block-formatting-context.html ------------------------------- OK
    floats-inside-overflow-scroll.html --------------------------------- OK
    floats-off-by-one.html --------------------------------------------- OK
    float-wrap.html ---------------------------------------------------- OK
    form-elements.html ------------------------------------------------- OK
    form-element-size.html --------------------------------------------- OK
    form-hidden-tab-assert.html ---------------------------------------- OK*

No problems with libcss, but this shows how tabbing between forms is
broken on GTK NS.

    form.html ---------------------------------------------------------- OK
    form-margins.html -------------------------------------------------- OK
    form-width.html ---------------------------------------------------- OK
    fractional-css-unit-test-cm.html ----------------------------------- OK
    fractional-css-unit-test-em.html ----------------------------------- OK
    frame1.html -------------------------------------------------------- N/A
    frame2.html -------------------------------------------------------- N/A
    frame3.html -------------------------------------------------------- N/A
    frames2.html ------------------------------------------------------- N/A
    frames.html -------------------------------------------------------- N/A

Frames are broken on GTK anyway.

    gaps-in-lists.html ------------------------------------------------- OK
    giftest.html ------------------------------------------------------- OK
    html-body-margin-padding-test.html --------------------------------- OK
    html-margin-body-margin-padding-test.html -------------------------- OK
    html-margin-padding-bgimage-body-margin-padding-bgimage-test.html -- OK
    html-margin-padding-bgimage-test.html ------------------------------ OK
    html-margin-padding-body-margin-padding-test.html ------------------ OK
    html-padding-body-margin-padding-test.html ------------------------- OK
    image-sizing.html -------------------------------------------------- OK
    inline-background.html --------------------------------------------- OK
    inline-background-position.html ------------------------------------ OK
    inline-block2.html ------------------------------------------------- OK
    inline-block3.html ------------------------------------------------- OK
    inline-block6.html ------------------------------------------------- OK
    inline-block.html -------------------------------------------------- OK
    inline-block-images.html ------------------------------------------- OK
    inline-test.html --------------------------------------------------- OK
    knockout.html ------------------------------------------------------ OK
    layout_line-floats2.html ------------------------------------------- OK
    layout_line-floats.html -------------------------------------------- OK
    link-underlines.html ----------------------------------------------- OK
    lists.html --------------------------------------------------------- OK
    margin-background1.html -------------------------------------------- OK
    margin-background2.html -------------------------------------------- OK
    margin-background3.html -------------------------------------------- OK
    margintest.html ---------------------------------------------------- OK
    metacharset.html --------------------------------------------------- OK
    min-height-in-float.html ------------------------------------------- OK
    minmax-height-1.html ----------------------------------------------- OK
    minmax-height-2.html ----------------------------------------------- OK
    minmax-height-3.html ----------------------------------------------- OK
    minmax-height-4.html ----------------------------------------------- OK
    minmax-height-5.html ----------------------------------------------- OK
    negative.html ------------------------------------------------------ OK
    negative-inline-margin.html ---------------------------------------- OK
    negative-margin-crash.html ----------------------------------------- OK
    negative-text-indent.html ------------------------------------------ OK
    netsurf-crash-1a.html ---------------------------------------------- OK
    netsurf-crash-1b.html ---------------------------------------------- OK
    netsurf-crash-2.html ----------------------------------------------- OK
    nocharset.html ----------------------------------------------------- OK
    nsgtk-redraw.html -------------------------------------------------- OK
    object-block.html -------------------------------------------------- OK
    object.html -------------------------------------------------------- OK
    overflow-auto-height.html ------------------------------------------ OK
    overflow.html ------------------------------------------------------ OK
    overflow-scrollbars.html ------------------------------------------- OK
    percentage-height-2.html ------------------------------------------- OK
    percentage-height-absolute-position-2.html ------------------------- OK
    percentage-height-absolute-position.html --------------------------- OK
    percentage-height-float-2.html ------------------------------------- OK
    percentage-height-float-3.html ------------------------------------- OK
    percentage-height-float.html --------------------------------------- OK
    percentage-height.html --------------------------------------------- OK
    percentage-height-inline-block-2.html ------------------------------ OK
    percentage-height-inline-block.html -------------------------------- OK
    place_float_below.html --------------------------------------------- OK
    relative.html ------------------------------------------------------ OK
    relative-position-and-floats.html ---------------------------------- OK
    relative-position-floats.html -------------------------------------- OK
    rowspan-overflow.html ---------------------------------------------- OK
    slashdot-overlap.html ---------------------------------------------- OK
    static-position.html ----------------------------------------------- OK
    stripped-down-bbc-test-layout.html --------------------------------- OK
    table-cell-width-and-height.html ----------------------------------- OK
    tablecollapse.html ------------------------------------------------- OK
    table-defaults.html ------------------------------------------------ OK
    table-form-breakage.html ------------------------------------------- OK
    tableseparate.html ------------------------------------------------- OK
    tags-before-body.html ---------------------------------------------- OK
    targets.html ------------------------------------------------------- N/A

Frames.

    textalign.html ----------------------------------------------------- OK
    unclickable-link.html ---------------------------------------------- OK
    w3c-links.html ----------------------------------------------------- OK
    wiki-test.html ----------------------------------------------------- OK
