[[!meta title="NetSurf 3.3"]]
[[!meta author="Kyllikki"]]
[[!meta date="2015-03-08T11:57:14Z"]]


[[!toc]] This page lists the main
goals for post NetSurf 3.2 development.

Important / Urgent
------------------

### Core

-   Remove utils/container.{c|h}
-   [[LibCSS style sharing|libcss_style_sharing]]
-   Make NetSurf use core buildsystem
-   fix css media queries

### GTK

-   Remove themes code

### Framebuffer

-   Get linux framebuffer surface working

Less Important / Less Urgent
----------------------------

### Javascript

-   nsgenbind DOM constructors
-   Make a decision on ducktape

