[[!meta title="NetSurf 2.5"]]
[[!meta author="Jmb"]]
[[!meta date="2010-04-19T20:56:42Z"]]


[[!toc]] This is a list of things
that need to be done before NetSurf 2.5 can be released.

Code
----

### Potential crashers

### Other

-   GTK: Local history window redraw is broken in areas you have to
    scroll to see

Admin
-----

-   Target frontends for release (RISC OS, GTK and Amiga OS 4.0)
-   [Changelog](http://www.netsurf-browser.org/temp/ChangeLog)
-   Testing
-   [Preparing release
    branch](http://source.netsurf-browser.org/branches/release/netsurf/2/ReleaseAdmin)
-   ISO Image for burning to CDs?

