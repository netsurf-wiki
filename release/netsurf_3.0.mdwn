[[!meta title="NetSurf 3.0"]]
[[!meta author="Tlsa"]]
[[!meta date="2013-04-08T14:35:32Z"]]


[[!toc]] This is a list of things
that still need to be fixed before NetSurf 3.0 can be released.

Code
----

### Potential crashers

-   ?

### Memory usage

-   ?

### Other

-   Buildsystem: Generate messages as part of build.
-   <s>Core: LibDOM: DOM events are a big performance regression.</s>
-   <s>Core: Remove last dependencies on LibXML.</s>
-   <s>Core: LibSVGTiny: Use LibDOM instead of LibXML.</s>
-   <s>GTK: Scrollwheel not working.</s>
-   <s>Buildsystem: We've all developed separate bash scripts to
    automate library fetch/update/build. Get this unified and into
    version control.</s>
-   <s>Documentation: Explain JS builds, their status, and what (not) to
    expect.</s>

### WONTFIX in 3.0

-   Core: Fix forms to use libdom properly. -- Postpone to 3.1.
-   Core: Fix entities getting separate BOX\_TEXT in box construction.
    -- Postpone to 3.1.
-   Core: Use new treeview. -- Postpone to 3.1.
-   Framebuffer: Linux surface support. -- Postpone to 3.1.
-   New logo? -- Postpone to 3.1.

Admin
-----

-   <strong>Can we automate production of releases?</strong>
-   Have any install targets broken? Missing resources? Trying to copy
    things that have gone?
-   Debian/Ubuntu packages

### Timeline

-   Branch in Apr 2013

### Target frontends for release

-   RISC OS
-   GTK
-   Amiga OS 4.0
-   Cocoa
-   Atari
-   ?

### Misc

-   For releases, turn Logging off in RISC OS front end. In
    !NetSurf.!Run: Set NetSurf\$Logging 0
-   [Changelog](http://www.netsurf-browser.org/temp/ChangeLog)
-   Testing
-   [Preparing release
    branch](http://source.netsurf-browser.org/branches/release/netsurf/2/ReleaseAdmin)
-   <strong>Old and out of date:</strong> Run the autobuilder. For
    example, with user:netsurf on semichrome.net:

<!-- -->

    $ svn export svn://svn.netsurf-browser.org/trunk/netsurfbuild netsurfbuild-v2.9
    $ cd netsurfbuild-v2.9
    $ svn co svn://svn.netsurf-browser.org/tags/hubbub/<version> hubbub
    $ svn co svn://svn.netsurf-browser.org/tags/libcss/<version> libcss
    $ svn co svn://svn.netsurf-browser.org/tags/libnsbmp/<version> libnsbmp
    $ svn co svn://svn.netsurf-browser.org/tags/libnsgif/<version> libnsgif
    $ svn co svn://svn.netsurf-browser.org/tags/libparserutils/<version> libparserutils
    $ svn co svn://svn.netsurf-browser.org/tags/libsvgtiny/<version> libsvgtiny
    $ svn co svn://svn.netsurf-browser.org/tags/libwapcaplet/<version> libwapcaplet
    $ svn co svn://svn.netsurf-browser.org/tags/netsurf/<version> netsurf
    $ svn co svn://svn.netsurf-browser.org/trunk/netsurfweb
    $ mkdir -p downloads/releases/
    $ ./autobuild.pl --release="2.9"
