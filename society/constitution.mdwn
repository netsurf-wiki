[[!meta title="Society/Constitution"]]
[[!meta author="Tlsa"]]
[[!meta date="2009-07-08T14:55:53Z"]]


[[!toc]] This is the constitution
of the NetSurf Society.

1.  This document details the constitution of the the NetSurf Project
    Society, hereinafter referred to as "the Society".
2.  Membership of the Society is non-transferable and is open to any
    person who fulfils any of the following:
    1.  The person is an active developer of any of the NetSurf
        Project's programmes or libraries or documentation or websites
        thereof. Such a person is defined by their having commit access
        to the NetSurf Project's central revision control system and
        having made a non-trivial commit within the past 12 months.
    2.  The person is an active developer as defined in A but, instead
        of having commit access to the NetSurf Project's central
        revision control system, their non-trivial updates, patches,
        modifications or new content is sponsored by an extant Member of
        the Society such that it is committed to the NetSurf Project's
        central revision control system on their behalf.

3.  Membership is free of charge, no subscription payments will be due.
4.  The Committee comprises three roles: Chair, Secretary and Treasurer.
5.  Membership applications and resignations must be made in writing
    (email or by post) or in person to the current holder of the
    Secretary role.
6.  The Secretary will accept and approve any such application providing
    the applicant fulfils any of the subclauses of 2. Resignation of
    membership is subject to no conditions for normal Members, and is
    predicated on a replacement having been elected for Committee
    Members.
7.  Membership is automatically terminated if any of the following are
    determined to be true:
    1.  The Member dies
    2.  The Member ceases to fulfil any of the requirements as laid out
        in the subclauses of 2.

8.  The Society's objects (the Objects) are:
    1.  To promote the use of, and advance the development of the
        NetSurf Web Browser
    2.  To promote the use of, and advance the development of the
        NetSurf Project's other applications and libraries.

9.  All income and property of the Society shall be applied towards
    furthering the Objects.
10. The Society will hold an annual general meeting (AGM) each year. At
    least 11 months and no more than 15 months may elapse between
    successive AGMs.
11. Any single member of the Committee, or 3 Members of the Society
    acting in concert may call an EGM at any time by requesting it of
    the Secretary in writing or in person.
12. No less than two weeks will elapse between the calling of a meeting
    (either AGM or EGM) and the meeting taking place.
13. Meetings may occur physically or in the IRC channel \#netsurf on the
    Libera Chat IRC network.
14. The Committee is elected at the AGM and the Committee Members serve
    the period starting immediately after the end of the AGM at which
    they were elected and ending at the end of the subsequent AGM.
15. Nominations for Committee roles and any motions to be considered at
    an AGM or EGM will be announced to the Members no more than one week
    and no less than four days before the meeting.
16. Motions and nominations for Committee members must be submitted to
    the Secretary after the announcement of the AGM (or an EGM called
    for the purpose of electing a new Committee member) and must arrive
    no less than one week before the meeting so that they can be
    included in the announcement detailed in 15. A candidate for a
    Committee role may withdraw themselves from consideration at any
    time up to the point at which the vote is called at the meeting.
    Should a given committee role not have any nominations by this time,
    or should all nominees withdraw before the vote, the incumbent is
    considered to be automatically re-nominated.
17. All Members of the Society have one vote.
18. Votes may only be cast by the Member unless previously delegated in
    writing to the Secretary.
19. Votes may be cast ahead of the meeting by providing a written
    account of the vote the Member wishes to cast to the Secretary no
    less than 24 hours ahead of the meeting.
20. A meeting is considered quorate if at least one Committee member is
    present and at least three other Members are present or have
    registered absent votes for all motions and nominations being
    considered.
21. Should a Committee member wish to resign, an EGM shall be called to
    elect a different Member to that role. The Member wishing to resign
    the role shall serve until such a meeting can take place. The newly
    elected Member will serve until the end of the subsequent AGM.
22. Should membership of the Society fall below five Members, or should
    it prove not possible to elect a Committee, then an EGM will be
    called to attempt to resolve the situation. Should the EGM end
    without membership increasing to at least five or without an elected
    Committee, then the Society will be dissolved.
23. Should a motion at an EGM or AGM be to dissolve the Society, then a
    two-thirds majority including at least one Committee Member will be
    required for the motion to pass.
24. Should the society be dissolved, the following must be fulfilled:
    1.  A resolution detailing the procedure for disposing of any assets
        belonging to the Society must be passed by a two-thirds majority
        of the remaining Members of the Society
    2.  The most recently elected Committee will then execute the
        resolution as passed.

25. No Member may simultaneously occupy more than one role on the
    Committee.
26. Signatories on any bank accounts held in the name of the Society
    shall be the members of the Committee.
27. Should the membership of the Committee change, it shall be the task
    of the Secretary (both old and new if changing) to ensure that the
    Bank remain informed of the permitted signatories and to ensure that
    any required paperwork is executed to that effect.
28. All votes are non-secret and all Members will be notified of the
    results of motions and nominations by the Secretary after any
    general meeting.
29. This Constitution may be amended by a motion passed by simple
    majority at any quorate general meeting.

