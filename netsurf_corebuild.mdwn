[[!meta title="NetSurf Corebuild"]]
[[!meta author="Kyllikki"]]
[[!meta date="2018-05-10T11:09:28Z"]]


[[!toc]]

## Makefiles

NetSurf itself does not use the core build system that all libraries
and tools do which must be fixed.

## Source layout

The source layout was in need of improvement to achive use of core buildsystem.

Most of the layout change has been done and current layout in May 2018 is:


    ├── content  
    │   ├── fetchers  
    │   └── handlers  
    │       ├── css  
    │       ├── html  
    │       ├── image  
    │       ├── javascript  
    │       └── text  
    ├── desktop  
    ├── docs  
    ├── frontends  
    │   ├── amiga  
    │   ├── atari  
    │   ├── beos  
    │   ├── framebuffer  
    │   ├── gtk  
    │   ├── monkey  
    │   ├── riscos  
    │   └── windows  
    ├── include  
    │   └── netsurf  
    ├── resources  
    │   ├── de  
    │   ├── en  
    │   ├── fr  
    │   ├── icons  
    │   ├── it  
    │   ├── ja  
    │   └── nl  
    ├── test   
    │   ├── data   
    │   └── js   
    └── utils  
        ├── http  
            └── nsurl  


vincents original target:

     + netsurf  
       + resources  
       + url  
       + utils  
       + test  
       + docs  
       + include  
       | + netsurf 
       + content  
       | + cache  
       | + fetchers  
       | + handlers  
       |   + css  
       |   + image  
       |   + html  
       |   + javascript  
       |   + text  
       + ui  
       | + browser  
       | + widgets  
       |   + textarea  
       |   + treeview  
       + frontends  
         + amiga  
         + atari  
         + beos  
         + cocoa  
         + framebuffer  
         + gtk  
         + monkey  
         + riscos  
         + windows


tlsa's proposal:


     + netsurf
       + test
       + docs
       + utils
       + core
       | + include
       | + content
       | | + cache
       | | + fetch
       | | + handlers
       | |   + css
       | |   + image
       | |   | + bmp
       | |   | + jpeg
       | |   | + gif
       | |   | + png
       | |   | + svg
       | |   + html
       | |   + javascript
       | |   + text
       | + ui
       | | + browser
       | | + widgets
       | |   + textarea
       | |   + treeview
       | + resources
       | + url
       | + utils
       + frontends
         + amiga
         + atari
         + beos
         + cocoa
         + framebuffer
         + gtk
         + monkey
         + riscos
         + windo


original layout in June 2016:


     + netsurf
       + resources
       + test
       + Docs
       + desktop
       + render
       + utils
       | + http
       + include
       | + netsurf
       + content
       | + fetchers
       | + handlers
       |   + image
       |   + css
       |   + javascript
       |     + duktape
       |     + none
       |     + WebIDL
       + frontends
         + amiga
         + atari
         + beos
         + cocoa
         + framebuffer
         + gtk
         + monkey
         + riscos
         + windo


Previous attempt:


     + netsurf
       + core
       | + api
       | + browser
       | + content
       | + css
       | + html
       | + image
       | + javascript
       | + resource
       | + widgets
       + frontends
       | + amiga
       | + atari
       | + beos
       | + cocoa
       | + framebuffer
       | + gtk
       | + monkey
       | + riscos
       | + windows
       + utils
       + test
       + do

